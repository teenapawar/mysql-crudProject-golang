package controllers

import (
	"encoding/json"
	"fmt"
	"gitlab.com/teenapawar/mysql-crudProject-golang/pkg/models"
	util "gitlab.com/teenapawar/mysql-crudProject-golang/pkg/utils"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var NewBook models.Book

func GetBooks(w http.ResponseWriter, r *http.Request) {
	NewBook := models.GetAllBooks()
	res, _ := json.Marshal(&NewBook)
	w.Header().Set("content.Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)

}

/* -----------or
func GetBooks(w http.ResponseWriter,r *http.Request){
	NewBook := models.GetAllBooks()
	w.Header().Set("content.Type","application/json")
	json.NewEncoder(w).Encode(NewBook)
	w.WriteHeader(http.StatusOK)

}
*/

func GetBookById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	bookId := params["bookid"]
	ID, err := strconv.ParseInt(bookId, 0, 0)
	if err != nil {
		fmt.Println("error in parsing")
	}
	bookDetails, _ := models.GetBookById(ID)
	res, _ := json.Marshal(&bookDetails)
	w.Header().Set("content.Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func CreateBook(w http.ResponseWriter, r *http.Request) {
	createBook := models.Book{}
	util.ParseBody(r, CreateBook)
	b:=createBook.CreateBook()
	res,_:= json.Marshal(b)
	w.Header().Set("content.Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(res)
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	bookId := params["bookid"]
	ID, err := strconv.ParseInt(bookId, 0, 0)
	if err != nil {
		fmt.Println("error in parsing")
	}
	bookDetails:=models.DeleteBook(ID)
	res,_:=json.Marshal(bookDetails)
	w.Header().Set("content.Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}
/* --------or----------------
func UpdateBook(w http.ResponseWriter, r *http.Request){
	UpdateBook:=models.Book{}
	util.ParseBody(r,UpdateBook)
	params := mux.Vars(r)
	bookId := params["bookid"]
	ID, err := strconv.ParseInt(bookId, 0, 0)
	if err != nil {
		fmt.Println("error in parsing")
	}
	_ = models.DeleteBook(ID)
	b:=UpdateBook.CreateBook()
	res,_:= json.Marshal(b)
	w.Header().Set("content.Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(res)
}*/
func UpdateBook(w http.ResponseWriter, r *http.Request){
	UpdateBook:=models.Book{}
	util.ParseBody(r,UpdateBook)
	params := mux.Vars(r)
	bookId := params["bookid"]
	ID, err := strconv.ParseInt(bookId, 0, 0)
	if err != nil {
		fmt.Println("error in parsing")
	}
	bookdetails,db := models.GetBookById(ID)
	if UpdateBook.Name !=""{
		bookdetails.Name=UpdateBook.Name
	}
	if UpdateBook.Author !=""{
		bookdetails.Author=UpdateBook.Author
	}
	if UpdateBook.Publication !=""{
		bookdetails.Publication=UpdateBook.Publication
	}
	db.Save(&bookdetails)
	res,_:= json.Marshal(bookdetails)
	w.Header().Set("content.Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(res)
}

