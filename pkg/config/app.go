package config

import(
	"github.com/jinzhu/gorm" 
	_"github.com/jinzhu/gorm/dialects/mysql"  //bank char and then link
)

var (
	db *gorm.DB	//init db as gorn.db type
)

func Connect(){
	/*// Configure the MySQL connection
    dsn := "user:password@tcp(localhost:3306)/database_name?charset=utf8mb4&parseTime=True&loc=Local"
	*/
	d,err:=gorm.Open("mysql","name:password/tablename?charset=utf8mb4&parseTime=True&loc=Local") //found db
	if err !=nil{
		panic(err)
	}
	db=d //assign empty db as mysql db
}

func GetDB() *gorm.DB{
	return db
}