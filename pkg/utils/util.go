package util

import(
	"encoding/json"
	"io/ioutil"
	"net/http"
)
//through create func we will get bosy which is in json 
//so we will parse (decode) it 
func ParseBody(r *http.Request,x interface{}){
	if body,err := ioutil.ReadAll(r.Body);err == nil{
		if err := json.Unmarshal([]byte(body),x);err !=nil{
			return 
		}
	} 
}
//--------------or-----------
// func ParseBody(r *http.Request, x interface{}) {
//     if err := json.NewDecoder(r.Body).Decode(x); err != nil {
//         return
//     }
// }